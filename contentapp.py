PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

ERROR_PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Error</h1>
    <p>Sorry, an error has occurred.</p>
  </body>
</html>
"""


import socket

class WebApp:

    def parse(self, request):
        """Parse the received request, extracting the relevant information."""

        print("Parse: Not parsing anything")
        return None

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Returns the HTTP code for the reply, and an HTML page.
        """

        print("Process: Returning 200 OK")
        return "200 OK", "<html><body><h1>It works!</h1></body></html>"

    def __init__(self, host, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((host, port))

        mySocket.listen(3)
        while True:
            print('Waiting for connections...')
            (recvSocket, address) = mySocket.accept()
            print('HTTP request received: ')
            request = recvSocket.recv(2048).decode('utf-8')
            print(request)

            petition = self.parse(request)
            (httpcode, html) = self.process(petition)

            response = "HTTP/1.1" + httpcode + "\r\n\r\n" + html + "\r\n"
            recvSocket.send(response.encode('utf-8'))
            recvSocket.close()



class ContentApp(WebApp):
    def __init__(self, host, port):
        self.content_dict = {"/": "<p>Main Source</p>", "/Sender": "<p>Nora</p>", "/Content": "<p>Have a nice day!</p>"}
        super().__init__(host, port)

    def parse(self, request):
        resource_name = request.split(' ', 2)[1]
        return resource_name

    def process(self, resource):
        # manejar las peticiones recibidas:
        # el recurso está ya almacenado:
        if resource in self.content_dict:
            http_code = "200 OK"
            cont = self.content_dict[resource]
            html_code = PAGE.format(content=cont)
        # caso contrario
        else:
            http_code = "404 Not found"
            html_code = ERROR_PAGE.format(resource=resource)
        return (http_code, html_code)

if __name__ == "__main__":
    my_web_app = ContentApp('localhost', 1243)










